#!/usr/bin/python3

import unittest

from schematic import *
from fields import *

from cli import Output as Out

class TestComponent(unittest.TestCase):
    def setUp(self):
        cmp_lines1 = [
            '$Comp',
            'L D_Small D2',
            'U 1 1 54D7A6A7',
            'P 7750 3300',
            'AR Path="/56223A3E/54D7A6A7" Ref="D2"  Part="1" ',
            'AR Path="/56223A52/54E9BE9F/54D7A6A7" Ref="D3"  Part="1" ',
            'AR Path="/56223A60/54E9BE9F/54D7A6A7" Ref="D4"  Part="1" ',
            'AR Path="/56223A6E/54E9BE9F/54D7A6A7" Ref="D5"  Part="1" ',
            'AR Path="/56223A7C/54E9BE9F/54D7A6A7" Ref="D6"  Part="1" ',
            'AR Path="/56223A90/54E9BE9F/54D7A6A7" Ref="D7"  Part="1" ',
            'AR Path="/56314410/54D7A6A7" Ref="D2"  Part="1" ',
            'F 0 "D2" H 7750 3400 50  0000 C CNN',
            'F 1 "1N4148" H 7750 3200 50  0000 C CNN',
            'F 2 "Diodes_SMD:SOD-123" H 7750 3300 60  0001 C CNN',
            'F 3 "http://www.diodes.com/_files/datasheets/ds30086.pdf" H 7750 3400 60  0001 C CNN',
                '1    7750 3300',
                '0    1    1    0   ',
            '$EndComp'
        ]
        cmp_lines2 = [
            '$Comp',
            'L R R4',
            'U 1 1 55F44BF3',
            'P 4700 4150',
            'AR Path="/56223A3E/55F44BF3" Ref="R4"  Part="1" ',
            'AR Path="/56223A52/54E9BE9F/55F44BF3" Ref="R9"  Part="1" ',
            'AR Path="/56223A60/54E9BE9F/55F44BF3" Ref="R14"  Part="1" ',
            'AR Path="/56223A6E/54E9BE9F/55F44BF3" Ref="R19"  Part="1" ',
            'AR Path="/56223A7C/54E9BE9F/55F44BF3" Ref="R24"  Part="1" ',
            'AR Path="/56223A90/54E9BE9F/55F44BF3" Ref="R29"  Part="1" ',
            'AR Path="/56314410/55F44BF3" Ref="R4"  Part="1" ',
            'F 0 "R4" V 4600 4150 50  0000 C CNN',
            'F 1 "100M" V 4700 4150 50  0000 C CNN',
            'F 2 "Resistors_SMD:R_0603" V 4630 4150 30  0001 C CNN',
            'F 3 "http://www.vishay.com/docs/20022/dcrcwhe3.pdf" H 4600 4150 60  0001 C CNN',
            'F 4 "Mouser" H 4600 4150 60  0001 C CNN "Supplier"',
            'F 5 "71-CRCW0603100MJPEA" H 4600 4150 60  0001 C CNN "Supplier Part Number"',
            'F 6 "http://eu.mouser.com/ProductDetail/Vishay-Dale/CRCW0603100MJPEAHR/?qs=sGAEpiMZZMu61qfTUdNhGwALI1ohWcNj4pxTQEkdYlc%3d" H 4600 4150 60  0001 C CNN "Supplier Link"',
            'F 7 "Vishay/Dale" H 4600 4150 60  0001 C CNN "Manufacturer"',
            'F 8 "CRCW0603100MJPEAHR" H 4600 4150 60  0001 C CNN "Manufacturer Part Number"',
            'F 9 "http://www.vishay.com/resistors-fixed/list/product-20011/" H 4600 4150 60  0001 C CNN "Manufacturer Link"',
                '1    4700 4150',
                '1    0    0    -1  ',
            '$EndComp'
        ]
        cmp_lines3 = [
            '$Comp',
            'L R R?',
            'U 1 1 56319048',
            'P 3750 6100',
            'F 0 "R?" V 3750 6100 50  0000 C CNN',
            'F 1 "20k" V 3750 6100 50  0000 C CNN',
            'F 2 "" V 3750 6100 30  0001 C CNN',
            'F 3 "" H 3750 6100 30  0001 C CNN',
                '1    3750 6100',
                '-1   0    0    1',
            '$EndComp'
        ]
        self.input_data = [cmp_lines1, cmp_lines2, cmp_lines3]

    def test_fields_length(self):
        for i in range(0, len(self.input_data)):
            component = Component(self.input_data[i])
            fields_len = [4,10,4]
            assert(len(component._fields) == fields_len[i])

    def test_serialize(self):
        """ Test reprodicibility """
        for i in range(0, len(self.input_data)):
            component = Component(self.input_data[i])
            assert(component.serialize() == component.serialize())

    def test_serialize_reparse_compare(self):
        for i in range(0, len(self.input_data)):
            component = Component(self.input_data[i])
            serialized = component.serialize()
            parsed_component = Component(serialized.splitlines())

            # compare native fields
            for key in ['Designator', 'Value', 'Footprint', 'Datasheet']:
                field = component.get_field(key)
                assert(parsed_component.get_field(key).matches(field.name, field.value))

            # compare serialized version
            assert(parsed_component.serialize() == '\n'.join(self.input_data[i]))

    def test_modify_value(self):
        for i in range(0, len(self.input_data)):
            component = Component(self.input_data[i])
            component.add_or_update_field('Value', '100n')
            assert(component.get_field('Value').value == '100n')

    def test_modify_value_manufacturer(self):
        for i in range(0, len(self.input_data)):
            component = Component(self.input_data[i])
            component.add_or_update_field('Value', '100n')
            component.add_or_update_field('Manufacturer', 'theFork electronics corp')
            assert(component.get_field('Value').value == '100n')
            assert(component.get_field('Manufacturer').value == 'theFork electronics corp')

    def test_merge(self):
        target_component = Component(self.input_data[2])
        source_component = Component(self.input_data[1])
        target_component.merge(source_component)
        assert(target_component.get_field('Value').value == '100M')
        assert(target_component.get_field('Value')._x_pos == 3750)
        assert(target_component.get_field('Supplier').value == 'Mouser')
        assert(target_component.get_field('Supplier')._x_pos == 3750)
        Out.test(target_component.serialize())

    def test_reset_fields(self):
        for i in range(0, len(self.input_data)):
            component = Component(self.input_data[i])
            component.reset_fields()
            assert(len(component._fields) == 4)
            assert(component.get_field('Designator').value != '')
            assert(component.get_field('Value').value == '')
            assert(component.get_field('Footprint').value == '')
            assert(component.get_field('Datasheet').value == '')
