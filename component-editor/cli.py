#!/usr/bin/python3

import re
import tempfile
import os
import datetime

from subprocess import call


class Output(object):

    """
    Offers standard output methods
    """

    @staticmethod
    def log_level():
        return 5

    @staticmethod
    def fine(string):
        if Output.log_level() > 6:
            print(string)

    @staticmethod
    def log(string):
        if Output.log_level() > 4:
            print('[%s] %s' % (Output.time(), string))

    @staticmethod
    def test(string):
        if Output.log_level() > 2:
            print(Styles.BLU + '[%s] %s' % (Output.time(), string), Styles.END)

    @staticmethod
    def ok(string):
        if Output.log_level() > 0:
            print(Styles.GRE + Styles.BOLD + '[%s] %s' % (Output.time(), string), Styles.END)

    @staticmethod
    def warn(string):
        print(Styles.YEL + Styles.BOLD + '[%s] WARNING: %s' % (Output.time(), string) + Styles.END)

    @staticmethod
    def fail(string):
        print(Styles.RED + Styles.BOLD+ '[%s] ERROR: %s' % (Output.time(), string) + Styles.END)

    @staticmethod
    def time():
        return '{0:%H:%M:%S}'.format(datetime.datetime.now())


class Styles:

    """
    Contains style constants for printing strings
    """
    BLU = '\033[94m'
    GRE = '\033[92m'
    YEL = '\033[93m'
    RED = '\033[91m'

    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    END = '\033[0m'

class Editor(object):

    """
    A helper class to call an editor, enter text and return the resulting string
    """

    def __init__(self, initial_content = ''):
        self.initial_content = initial_content
        self.editor = os.environ.get('EDITOR', 'vim')
        self.encoding = 'UTF-8'

    def run(self):
        """ Run editor and return file content """
        with tempfile.NamedTemporaryFile(suffix='.tmp') as temp_file:
            # write template
            temp_file.write(bytes(self.initial_content, self.encoding))
            temp_file.flush()

            # call editor
            call([self.editor, temp_file.name])

            # read file
            temp_file.seek(0)
            file_content = temp_file.read().decode(self.encoding)
        return file_content

    def parse(self, file_content):
        """ Parse a key-value-pair structured string and return a dictionary.
            Lines starting with a hash or ending with an asterisk are ignored. """

        dictionary = {}
        for line in file_content.splitlines():
            # ignore comments, wildcards and empty lines
            if re.match('^#', line):
                continue
            elif re.match('^(.*)= \*', line):
                continue
            elif line == '':
                continue

            # parse lines and fill dictionary
            try:
                name = line.split('\'')[1].strip()
                value = line[line.index('=')+1:].strip()
                dictionary[name] = value
            except IndexError:
                Output.warn('Invalid syntax in line \"' + line + '\". Will ignore this field')

        return dictionary
