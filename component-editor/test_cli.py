#!/usr/bin/python3

import unittest
from mock import MagicMock

from cli import *

class TestCli(unittest.TestCase):
    def setUp(self):
        self.editor = Editor('initial content\nsecond line')

    def test_logger(self):
        Output.log('Normal operation')
        Output.log('Apparently running')
        Output.log('Still running')
        Output.warn('Well this is embarrassing...')
        Output.log('Advanced black magic ;)')
        Output.fail('Ouch - that should not happen!')

    def test_editor(self):
        self.editor.run = MagicMock(return_value = self.editor.initial_content)
        return_string = self.editor.run()
        assert(return_string == self.editor.initial_content)

    def test_parser(self):
        dictionary = self.editor.parse('\'Datasheet\' = http://eu.mouser.com/ProductDetail/Omron-Electronics/G6K-2P-Y-DC5/?qs=sGAEpiMZZMs3UE%252bXNiFaVELplwtkvP6Y68aMqtMDJ8w%3d')
        assert(dictionary['Datasheet'] == 'http://eu.mouser.com/ProductDetail/Omron-Electronics/G6K-2P-Y-DC5/?qs=sGAEpiMZZMs3UE%252bXNiFaVELplwtkvP6Y68aMqtMDJ8w%3d')
