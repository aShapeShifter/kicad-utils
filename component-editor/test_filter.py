#!/usr/bin/python3
import unittest

from schematic import *
from component import *
from fields import *
from cli import Output as Out

class TestFilter(unittest.TestCase):

    def setUp(self):
        Out.log('Preparing components using schematic class')
        self.test_schematic = Schematic('test1.sch')
        self.test_components = self.test_schematic._components
        self.test_filter = Filter()

    def test_1(self):
        """ Filter value 100n """

        self.test_filter.add_constraint('Value','100n')

        matching_components = []
        for comp in self.test_components:
            if self.test_filter.matches(comp):
                matching_components.append(comp)

        assert(len(matching_components) == 2)

    def test_2(self):
        """ Filter custom field manufacturer """

        Out.log('Generating filter for a given manufacturer')
        self.test_filter = Filter()
        self.test_filter.add_constraint('Manufacturer','Vishay/Dale')

        matching_components = []
        for comp in self.test_components:
            if self.test_filter.matches(comp):
                matching_components.append(comp)

        Out.log('Matching components: %s' % matching_components)
        assert(len(matching_components) == 5)

    def test_3(self):
        """ Add a second field to the filter """

        Out.log('Adding a second field (value) to the filter')
        self.test_filter.add_constraint('Value','180')
        self.test_filter.add_constraint('Manufacturer','Vishay/Dale')
        matching_components = []
        for comp in self.test_components:
            if self.test_filter.matches(comp):
                matching_components.append(comp)

        Out.log('Matching components: %s' % matching_components)
        assert(len(matching_components) == 1)
