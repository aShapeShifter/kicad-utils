# Component Editor
This tool offers component field editing on multi-schematic level. By means of a filter a set of components is selected from the schematic(s).
After displaying the matching components, the user can modify fields and apply them to all matching components.

### Requirements
* python3
* python3-pytest
* python3-mock

### Usage
```
$./component_editor.py -h
usage: component_editor.py [-h] [-v VALUE] [-f FOOTPRINT] [-i]
                           SCHEMATIC [SCHEMATIC ...]

Edit attributes of multiple components in multiple schematic files

positional arguments:
  SCHEMATIC             Schematic files to parse, filter and edit

optional arguments:
  -h, --help            show this help message and exit
  -v VALUE, --value VALUE
                        Component value filter
  -f FOOTPRINT, --footprint FOOTPRINT
                        Component value filter
  -i, --inplace         Overwrite schematics instead of adding .edit files

```

# Planned features / TODO
Roughly ordered by importance:

### Diff
* Print a human-readable diff for schematics, then ask before writing

### Tests
* Add further tests on component editor level
* Is it possible to run eeschema to verify created schematics?

### Component files
* Load component fields from dedicated component files
* Create component files from schematics
* Search for existing component files after filter entry

# Known Bugs

### 1
Fields deleted in component editor are deleted only in the first component (matching\_components[0]).
Desired behaviour: Field deleted in the editor should disappear in all components.
