#!/usr/bin/python3

import os.path
import sys

from component import *
from filter import *
from cli import Output as Out

class Schematic(object):
    """
    Represents a schematic file.
    Handles loading, parsing, serialization, component filtering and modification
    """

    # kicad schematic syntax
    BEGIN_COMPONENT = '$Comp'
    END_COMPONENT = '$EndComp'
    END_SCHEMATIC = '$EndSCHEMATC'

    def __init__(self, filename):
        self.filename = filename

        # contains component object and non-component content as strings
        self._components = []

        self._original_content = []
        self._new_content = []

        # the header contains everything before the first component
        self._header = []

        # the trailer contains everything after the last component
        self._trailer = []

        self._load()
        self._parse()

    def __str__(self):
        return 'todo'

    def _load(self):
        """ Load the file to original content member """

        if os.path.isfile(self.filename):
            with open(self.filename, 'r') as input_file:
                self._original_content = input_file.read().splitlines()
        else:
            Out.fail('File ' + self.filename + ' not found.')
            sys.exit()

    def _parse(self):
        """ Parse original content, store it to header, trailer and components list """

        line_number = 0

        # store header string
        while self._original_content[line_number] != self.BEGIN_COMPONENT:
            self._header.append(self._original_content[line_number])
            line_number += 1

        # create component list, store everything outside components to the trailer
        in_component = False
        while self._original_content[line_number] != self.END_SCHEMATIC:
            line = self._original_content[line_number]

            # write line to current component or trailer list
            if in_component:
                current_component.append(line)
            elif line != self.BEGIN_COMPONENT:
                self._components.append(line)

            # detect component begin
            if line == self.BEGIN_COMPONENT:
                in_component = True
                current_component = [line]

            # create component object when detecting component end
            elif line == self.END_COMPONENT:
                in_component = False
                self._components.append(Component(current_component))

            line_number += 1

        self._trailer.append(self.END_SCHEMATIC)

    def filter_components(self, component_filter):
        """ Returns all components that matches to given filter """

        matching_components = []
        for comp in self._components:
            if type(comp) is not Component:
                continue
            elif component_filter.matches(comp):
                matching_components.append(comp)

        return matching_components

    def serialize(self):
        """ Returns a string containing the content of the (new) schematic file """

        line_separator = '\n'

        # create new_content list
        self._new_content = list(self._header)
        for comp in self._components:
            if type(comp) is Component:
                self._new_content.append(comp.serialize())
            else:
                self._new_content.append(comp)
        self._new_content.extend(self._trailer)

        # return string of new_content
        return line_separator.join(self._new_content) + line_separator

    def write(self, inplace=False):
        """ Writes the current state to the (eventually given) filename """

        # determine filename
        if not inplace:
            filename = self.filename + '.edit'
            Out.ok('Writing schematic to ' + filename)
            Out.warn('Remember to overwrite the original file before making further changes.')
        else:
            filename = self.filename
            Out.ok('Writing schematic to ' + filename)

        # write
        with open(filename,'w') as out_file:
            out_file.write(self.serialize())
