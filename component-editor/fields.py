#!/usr/bin/python3

import re

from abc import ABCMeta, abstractmethod
from cli import Output as Out

class Field(object, metaclass=ABCMeta):
    """
    This class represents a single field in a schematic file.
    The class has subtypes for the two types of fields:
      * "Native" fields defined and used by Kicad and
      * "Custom" fields defined by the user.
    """

    known_names = ['Designator','Value','Footprint','Datasheet',
                   'Supplier','Supplier Part Number','Supplier Link',
                   'Manufacturer', 'Manufacturer Part Number']

    def __init__(self):
        self.name = ''
        self.value = ''
        self.number = -1

        self._orientation = 'X'
        self._x_pos = -1
        self._y_pos = -1
        self._size = -1
        self._flags = '0000' # apparently, atm the flags only indicate visibility
        self._text_align = 'X'
        self._cnn = 'CNN'# ;)

    def __str__(self):
        """ Returns a string representation of the field for debug purposes. """
        return '%-32s:\t%s' % (self.name, self.value)

    def __repr__(self):
        """ Returns a shorter string representation for printing lists of fields """
        return '%s:%s' % (self.name, self.value)

    def _parse_common(self, line):
        """ Performs parsing steps that are common to native and custom fields """
        assert(type(line) is str)

        # parse value
        tokens = line.split('"')
        self.value = tokens[1]

        # parse positions, flags, ...
        tokens = tokens[2].split(' ')
        self._orientation = tokens[1]
        self._x_pos = int(tokens[2])
        self._y_pos = int(tokens[3])
        self._size = int(tokens[4])
        self._flags = str(tokens[6])
        self._text_align = str(tokens[7])
        self._cnn = str(tokens[8])

    def matches(self, name, value):
        """ Return true if both name and value are equal """

        if self.name == name:
            if self.value == value:
                return True
            else:
                if self.value == value:
                    # exact match
                    return True
                else:
                    # check regex match
                    # value may contain a regex, i.e. R[1-3] for matching R1, R2 and R3
                    pattern = re.compile(value)
                    if pattern.match(self.value) != None:
                        Out.fine('Detected field match using regular expression: ' + value)
                        return True
        else:
            return False

    def set_visible(self, visibile):
        """ Set whether a field should be visible in the schematic editor """

        if visibile:
            self._flags = '0000'
        else:
            self._flags = '0001'

    @abstractmethod
    def serialize(self):
        pass


class NativeField(Field):
    """
    This class represents a "native" field in a schematic file as used by Kicad, namely
      * A designator,
      * a value,
      * a footprint or
      * a link to the datasheet.
    """

    field_numbers = {
        0: "Designator",
        1: "Value",
        2: "Footprint",
        3: "Datasheet",
    }

    @classmethod
    def parse(cls, line):
        """ Parses a 'F ...' line of a schematic file and extracts the field information. """
        assert(type(line) is str)

        # initialize field and parse common fields
        field = cls()
        field._parse_common(line)

        # determine name using predefined field numbers
        tokens = line.split('"')
        tokens = tokens[0].split()
        field.number = int(tokens[1])
        field.name = cls.field_numbers.get(field.number)

        return field

    def serialize(self):
        """ Serializes the field information in the schematic file format. """
        return 'F %d "%s" %s %d %d %d  %s %s %s' \
             % (self.number, self.value,
                self._orientation, self._x_pos, self._y_pos, self._size, self._flags, self._text_align, self._cnn)


class CustomField(Field):
    """
    This class represents a "custom" field in a schematic file.
    These fields can be defined by the user.
    """

    @classmethod
    def copy(cls, source_field):
        """ Returns a copy of the specified field as an instance of CustomField """
        assert(isinstance(source_field, Field))

        new_field = cls()
        new_field._name = source_field.name
        new_field.value = source_field.value
        new_field.number = source_field.number

        new_field._x_pos = source_field._x_pos
        new_field._y_pos = source_field._y_pos
        new_field._orientation = source_field._orientation
        new_field._size = source_field._size
        new_field._flags = source_field._flags
        new_field._text_align = source_field._text_align
        new_field._cnn = source_field._cnn
        return new_field

    @classmethod
    def parse(cls, line):
        """ Parses an 'F ...' line of a schematic file and extracts the field information. """
        assert(type(line) is str)

        # initialize field and parse common fields
        field = cls()
        field._parse_common(line)

        # copy name
        tokens = line.split('"')
        field.name = tokens[3]

        # extract number
        num_tokens = tokens[0].split(' ')
        field.number = int(num_tokens[1])

        # custom fields are always invisible
        field._flags = '0001'

        return field

    def serialize(self):
        """ Serializes the field information in the schematic file format. """
        return 'F %d "%s" %s %d %d %i  %s %s %s "%s"' \
             % (self.number, self.value,
                self._orientation, self._x_pos, self._y_pos, self._size, self._flags, self._text_align, self._cnn,
                self.name)


class FieldParser():
    @staticmethod
    def parse(line):
        assert(type(line) is str)
        if re.match('^F [0-3] "', line): # native field
            Out.fine('Parsing native field: ' + line)
            return NativeField.parse(line)
        else: # custom field
            Out.fine('Parsing custom field: ' + line)
            return CustomField.parse(line)
