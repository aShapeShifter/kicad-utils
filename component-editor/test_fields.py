#!/usr/bin/python3
import unittest

from fields import *

class TestFields(unittest.TestCase):

    def setUp(self):
        self.field_strings = []
        self.field_strings.append('F 0 "REL1" H 3700 4000 60  0000 C CNN')
        self.field_strings.append('F 1 "Omron G6K-2P-5VCD" H 3700 3500 60  0001 C CNN')
        self.field_strings.append('F 2 "uMIDI-switcher:Omron_G6K-2P" H 3700 3800 60  0001 C CNN')
        self.field_strings.append('F 3 "https://www.omron.com/ecb/products/pdf/en-g6k.pdf" H 3700 4000 60  0001 C CNN')
        self.field_strings.append('F 4 "Mouser" H 3700 4000 60  0001 C CNN "Supplier"')
        self.field_strings.append('F 5 "653-G6K-2P-Y-DC5" H 3700 4000 60  0001 C CNN "Supplier Part Number"')
        self.field_strings.append('F 6 "http://eu.mouser.com/ProductDetail/Omron-Electronics/G6K-2P-Y-DC5/?qs=sGAEpiMZZMs3UE%252bXNiFaVELplwtkvP6Y68aMqtMDJ8w%3d" H 3700 4000 60  0001 C CNN "Supplier Link"')
        self.field_strings.append('F 7 "Omron" H 3700 4000 60  0001 C CNN "Manufacturer"')
        self.field_strings.append('F 8 "G6K-2P-Y-DC5" H 3700 4000 60  0001 C CNN "Manufacturer Part Number"')

    def test_parse_serialize(self):
        for index, field_string in enumerate(self.field_strings):
            if index <= 3:
                test_field = NativeField.parse(field_string)
            else:
                test_field = CustomField.parse(field_string)

            assert(test_field.serialize() == field_string)
