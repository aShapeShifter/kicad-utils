EESchema Schematic File Version 2
LIBS:uMIDI-volume-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:max1044
LIBS:stacked_jack
LIBS:relais
LIBS:uMIDI-volume-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1250 1250 1200 1200
U 54D12158
F0 "Power Supply" 60
F1 "power.sch" 60
F2 "9V_Wah" O R 2450 1450 60 
F3 "-6V_VCA" O R 2450 1800 60 
F4 "6V_VCA" O R 2450 1700 60 
$EndSheet
$Sheet
S 5650 3150 1050 1500
U 54EA3170
F0 "VCA" 60
F1 "vca.sch" 60
F2 "Output" O R 6700 3250 60 
F3 "Input" I L 5650 3250 60 
F4 "D6" I L 5650 3450 60 
F5 "D5" I L 5650 3550 60 
F6 "D4" I L 5650 3650 60 
F7 "D3" I L 5650 3750 60 
F8 "D2" I L 5650 3850 60 
F9 "D1" I L 5650 3950 60 
F10 "D0" I L 5650 4050 60 
$EndSheet
$Sheet
S 5500 2100 1300 650 
U 56314410
F0 "True Bypass" 60
F1 "bypass.sch" 60
F2 "Loop_In" I L 5500 2200 60 
F3 "Loop_Out" O R 6800 2200 60 
F4 "FX_Send" O L 5500 2600 60 
F5 "FX_Return" I R 6800 2600 60 
F6 "Ctrl" I L 5500 2400 60 
$EndSheet
$Comp
L CONN_02X05 X?
U 1 1 56324CAD
P 4500 3650
F 0 "X?" H 4500 3950 50  0000 C CNN
F 1 "uMIDI" H 4500 3350 50  0000 C CNN
F 2 "Connect:VASCH5x2" H 4500 2450 60  0001 C CNN
F 3 "" H 4500 2450 60  0001 C CNN
	1    4500 3650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5632509C
P 4850 3950
F 0 "#PWR?" H 4850 3700 50  0001 C CNN
F 1 "GND" H 4850 3800 50  0000 C CNN
F 2 "" H 4850 3950 60  0001 C CNN
F 3 "" H 4850 3950 60  0001 C CNN
	1    4850 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2600 5400 2600
Wire Wire Line
	5400 2600 5400 3250
Wire Wire Line
	5400 3250 5650 3250
Wire Wire Line
	6700 3250 6900 3250
Wire Wire Line
	6900 3250 6900 2600
Wire Wire Line
	6900 2600 6800 2600
Wire Wire Line
	4750 3850 4850 3850
Wire Wire Line
	4850 3850 4850 3950
Wire Wire Line
	4750 3450 5650 3450
Wire Wire Line
	4250 3550 4250 3500
Wire Wire Line
	4250 3500 5550 3500
Wire Wire Line
	5550 3500 5550 3550
Wire Wire Line
	5550 3550 5650 3550
Wire Wire Line
	4750 3550 5450 3550
Wire Wire Line
	5450 3550 5450 3650
Wire Wire Line
	5450 3650 5650 3650
Wire Wire Line
	4250 3650 4250 3600
Wire Wire Line
	4250 3600 5350 3600
Wire Wire Line
	5350 3600 5350 3750
Wire Wire Line
	5350 3750 5650 3750
Wire Wire Line
	4750 3650 5250 3650
Wire Wire Line
	5250 3650 5250 3850
Wire Wire Line
	5250 3850 5650 3850
Wire Wire Line
	4250 3750 4250 3700
Wire Wire Line
	4250 3700 5150 3700
Wire Wire Line
	5150 3700 5150 3950
Wire Wire Line
	5150 3950 5650 3950
Wire Wire Line
	4750 3750 5050 3750
Wire Wire Line
	5050 3750 5050 4050
Wire Wire Line
	5050 4050 5650 4050
Wire Wire Line
	4250 3850 4050 3850
Wire Wire Line
	4050 3850 4050 2400
Wire Wire Line
	4050 2400 5500 2400
NoConn ~ 4250 3450
$EndSCHEMATC
