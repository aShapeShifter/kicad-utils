#!/usr/bin/python3
import unittest

from schematic import *

from filter import *
from cli import Output as Out

class TestSchematic(unittest.TestCase):

    def setUp(self):
        self.test_schematics = []
        self.serialized_schematics = []
        self.test_files = ['test1.sch', 'test2.sch', 'test3.sch', 'test4.sch']
        self.matching_100n = [2,9,1,0]
        self.matching_100n_0603 = [2,0,0,0]

        # init and serialize all schematics
        for i in range(0, len(self.test_files)):
            test_file = self.test_files[i]
            Out.test('Initialize and serialize schematic file ' + test_file + ' ...')
            self.test_schematics.append(Schematic(test_file))
            self.serialized_schematics.append(self.test_schematics[i].serialize())


        # init filter
        self.test_filter = Filter()

    def test_serialize(self):

        for i in range(0, len(self.test_files)):

            # load original file and get length
            original = ''
            with open(self.test_files[i], 'r') as test_file_handle:
                for line in test_file_handle:
                    original += line

            # compare content
            assert(self.serialized_schematics[i] == original)


    def test_filter_value(self):

        for i in range(0, len(self.test_files)):
            # filter all 100n capacitors
            Out.test('Counting matches of value=100n')
            self.test_filter.add_constraint('Value', '100n')
            matching = self.test_schematics[i].filter_components(self.test_filter)
            assert(len(matching) == self.matching_100n[i])

    def test_filter_value_and_footprint(self):

        for i in range(0, len(self.test_files)):
            # filter all 100n capacitors with a certain footprint
            Out.test('Counting matches of value=100n and footprint=Capacitors_SMD:C_0603')
            self.test_filter.add_constraint('Value', '100n')
            self.test_filter.add_constraint('Footprint', 'Capacitors_SMD:C_0603')
            matching = self.test_schematics[i].filter_components(self.test_filter)
            assert(len(matching) == self.matching_100n_0603[i])
