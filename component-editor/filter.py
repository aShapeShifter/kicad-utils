#!/usr/bin/python3

from schematic import *
from component import *
from cli import Output as Out
from cli import Editor

class Filter(object):

    def __init__(self):
        self._constraints = {}

    def add_constraint(self, name, value):
        self._constraints[name] = value

    def matches(self, component):
        """ Return true if all fields of the given component are equal """

        # arrays to store booleans whether a field exist and matches the filter
        found = []
        matches = []

        # return false if no component given
        if type(component) is not Component:
            return False

        # check all fields of the filter and fill aforementioned arrays
        for name, value in self._constraints.items():
            component_field = component.get_field(name)

            if component_field is None:
                found.append(False)
            elif component_field.matches(name, value):
                found.append(True)
                matches.append(True)
            else:
                found.append(True)
                matches.append(False)

        # evaluate found and matches arrays
        for i in range(0, len(self._constraints)):
            if not found[i] or not matches[i]:
                return False
        return True

    def edit(self):
        """ Open an editor to edit the filter """

        if len(self._constraints) != 0:
            Out.warn('Editing filters that already have constraints is not yet supported')

        # create initial content
        initial_content = '# FILTER EDITOR\n'
        initial_content += '# - an asterisk (*) indicates "dont-care" fields\n'
        initial_content += '# - you may either\n'
        initial_content += '#    - simply specify a field such as value = 100n, or\n'
        initial_content += '#    - you can use regular expressiong i.e. ^R for getting all resistors\n\n'
        for field_name in Field.known_names:
            initial_content += '\'' + field_name + '\' = *\n'

        # run editor
        editor = Editor(initial_content)
        filter_file_content = editor.run()

        # parse content and add fields
        fields_dict = editor.parse(filter_file_content)
        for name, value in fields_dict.items():
            self.add_constraint(name, value)
