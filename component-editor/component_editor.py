#!/usr/bin/python3

import argparse
import sys

from schematic import *
from filter import *
from cli import Output as Out

class ComponentEditor(object):
    """
    The component editor main class
    """

    def __init__(self, schematic_filenames, value=None, footprint=None):
        self.schematic_filenames = schematic_filenames
        self.value = value
        self.footprint = footprint
        self.component_filter = Filter()
        self.schematics = []

        self.matching_components = []
        self.component_template = None

    def read_schematics(self):
        # create schematic instances
        for filename in self.schematic_filenames:
            self.schematics.append(Schematic(filename))

    def query_filters(self):
        # query filters if not already given in self
        if self.footprint is None and self.value is None:
            # run filter editor
            self.component_filter.edit()
        else:
            # set from self
            self.set_filters(self.value, self.footprint)

    def verify_matches(self):
        """ Show matches and ask to continue """
        self.print_matches()
        if not self.confirm('Proceed?'):
            Out.ok('Bye!')
            sys.exit()

    def edit_component_template(self):
        """ Edit first matching component """
        self.component_template = self.matching_components[0]
        self.component_template.edit()
        Out.fine('Component template:\n' + self.component_template.serialize())

    def merge_component_template(self):
        """ Merge component template into all matching components """
        for comp in self.matching_components:
            comp.merge(self.component_template)

    def set_filters(self, value='', footprint=''):
        """ Set a filter only by value and footprint """
        if value != '' and value is not None:
            self.component_filter.add_constraint('Value', value)
        if footprint != '' and footprint is not None:
            self.component_filter.add_constraint('Footprint', footprint)

    def apply_filters(self):
        """ Determine the components matching against current filter """
        for schematic in self.schematics:
            matching_components = schematic.filter_components(self.component_filter)
            if len(matching_components) != 0:
                self.matching_components.extend(matching_components)

    def print_matches(self):
        """ Print all matching components """
        Out.ok('Matching components:')
        for component in self.matching_components:
            Out.log(component)

    def write_schematics(self, inplace=False):
        """ Parse schematics and write them back """
        for schematic in self.schematics:
            schematic.write(inplace)

    def confirm(self, question):
        proceed_char = input(question + ' [y/N]')
        if proceed_char != 'y' and proceed_char != 'Y':
            return False
        else:
            return True


if __name__ == '__main__':

    # parse arguments
    parser = argparse.ArgumentParser(description='Edit attributes of multiple ' +
                                     'components in multiple schematic files')
    parser.add_argument('schematics', metavar='SCHEMATIC', nargs='+',
                        help='Schematic files to parse, filter and edit')

    parser.add_argument('-v','--value', help='Component value filter')
    parser.add_argument('-f', '--footprint', help='Component value filter')
    parser.add_argument('-i', '--inplace', help='Overwrite schematics instead of adding .edit files', action='store_true')
    args = parser.parse_args()

    # init component editor
    component_editor = ComponentEditor(args.schematics, args.value, args.footprint)

    # run component editor
    component_editor.read_schematics()
    component_editor.query_filters()
    component_editor.apply_filters()
    component_editor.verify_matches()
    component_editor.edit_component_template()
    component_editor.merge_component_template()
    component_editor.write_schematics(args.inplace)
