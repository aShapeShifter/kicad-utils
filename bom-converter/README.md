# Bom-Converter

A script to generate a pretty bill of material in HTML format. The input file in xml-format is created using the  eeschema bom creator without a plugin.

```
usage: bom-converter [-h] [--csv [CSV_OUTPUT]] infile [htmlfile]

Converts a kicad bom given in XML format to HTML.

positional arguments:
  infile              XML file exported by eeschema (kicad)
  htmlfile            HTML output file

optional arguments:
  -h, --help          show this help message and exit
  --csv [CSV_OUTPUT]  CSV output file
```

Example usage:

```
$ ./bom-converter.py test.xml bom.html
```
